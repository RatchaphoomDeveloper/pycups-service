from django.db import models

class FileLogging(models.Model):
    filename = models.CharField(max_length=1024)
    dateGroup = models.CharField(max_length=1024)
    message = models.CharField(max_length=1024)

    # filepath = models.CharField(max_length=1024)

class PrintLogging(models.Model):
    filename = models.CharField(max_length=1024)
    create_date = models.CharField(max_length=1024)
    message = models.CharField(max_length=1024)


# def __str__(self):
#         return self.filename

# Create your models here.
