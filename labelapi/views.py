from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from blabel import LabelWriter
import base64
import os
from datetime import datetime
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import viewsets
from .models import FileLogging,PrintLogging
import cups
import json
import requests
from .serializers import FileLoggingSerializers,PrintLoggingSerializers
import time
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
# Create your views here.

class FileLoggingView(viewsets.ModelViewSet):
    queryset = FileLogging.objects.all();
    serializer_class = FileLoggingSerializers


class PrintLoggingView(viewsets.ModelViewSet):
    queryset = PrintLogging.objects.all();
    serializer_class = PrintLoggingSerializers;



@api_view(['POST'])
def barcodeGenerate(request):
    try:
        data = request.data
        records = []
        label_writer = LabelWriter(
        os.path.join(APP_ROOT, '../templates/item_template.html'),
        default_stylesheets=(os.path.join(
                    APP_ROOT, '../templates/style.css'),),
            )
        count = 0
        for x in request.data:
            zplText = "^XA"+"^CF0,60"+"^FO220,50^FD"+"DATASAFE"+"^FS"+"^FO50,125^BY3,2.0,75^BC,130,N,N^FD"+x['sample_id']+"^FS"+"^FS"+"^FO140,260"+"^A0N,40,50"+"^FD"+x['sample_id']+"^FS"+"^XZ"
            f = open(os.path.join(APP_ROOT,'../pdf/label'+str(count)+'.zpl'), "w")
            f.write(zplText)
            f.close()
            time.sleep(3)
            count += 1
        # pathName = "../pdf/barcode"+ datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+".pdf"
        # dateTimeGroup = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        # label_writer.write_labels(records, target=os.path.join(APP_ROOT, pathName))
        # requests.post('http://localhost:8000/api/filelogging/filelogging/',data={
        # "filename": pathName,
        # "dateGroup": dateTimeGroup,
        # "message":"success"
        # })
        return JsonResponse({"status": 200, "message":"success"}, safe=False)
    except Exception as e:
        print(e.__class__)
        requests.post('http://localhost:8000/api/filelogging/filelogging/',data={
        "filename": pathName,
        "dateGroup": dateTimeGroup,
        "message":"error"
        })
    return JsonResponse({"status": 200, "message":"fail"}, safe=False)



@api_view(['POST'])
def barcodePrint(request):
    pathName = "../pdf/barcode"+ datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+".pdf"
    dateTimeGroup = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    try:
        conn = cups.Connection (host="172.16.86.23",port=631)
        printers = conn.getPrinters ()
        for printer in printers:
            print (printer, printers[printer]["device-uri"])
            job_id = conn.createJob(printer, '', {})
            prin = conn.getDefault()
            count = 0
            zplText = ""
            fail = ""
            for x in request.data:
                if len(x['sample_id']) > 20:
                 fail = "ไม่สามารถปริ้นได้ (ขนาดตัวอักษรยาวเกินไป)"
                if len(x['sample_id']) < 10:
                    zplText = "^XA"+"^CF0,60"+"^FO"+("275" if x['sample_name'] == "" else "275")+",25^FD"+("DATASAFE" if x['sample_name'] == "" else x['sample_name'] )+"^FS"+"^FO150,75^BY4,2.0,80^BC,130,N,N^FD"+x['sample_id']+"^FS"+"^FS"+"^FO"+("273" if x['sample_name'] == "" else "250")+",220"+"^A0N,80,70"+"^FD"+x['sample_id']+"^FS"+"^XZ"
                else:
                    zplText = "^XA"+"^CF0,60"+"^FO"+("275" if x['sample_name'] == "" else "275")+",25^FD"+("DATASAFE" if x['sample_name'] == "" else x['sample_name'] )+"^FS"+"^FO200,75^BY3.2,2.0,80^BC,130,N,N^FD"+x['sample_id']+"^FS"+"^FS"+"^FO"+("273" if x['sample_name'] == "" else "250")+",220"+"^A0N,80,70"+"^FD"+x['sample_id']+"^FS"+"^XZ"


                f = open(os.path.join(APP_ROOT,'../pdf/label.zpl'), "w")
                f.write(zplText)
                f.close()
                time.sleep(0)
                conn.printFile(printer, os.path.join(APP_ROOT,"../pdf/label.zpl"),"label.zpl",{})
                count += 1
        # pathName = "../pdf/barcode"+ datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+".pdf"
        dateTimeGroup = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        requests.post('http://localhost:8000/api/filelogging/printlogging/',data={
            "filename": "label.zpl",
            "create_date": dateTimeGroup,
            "message":"success"
         })
        return JsonResponse({"status": 200, "message": fail if fail == "" else "success"}, safe=False)
    except:
         requests.post('http://localhost:8000/api/filelogging/printlogging/',data={
            "filename": pathName,
            "create_date": dateTimeGroup,
            "message":"error"
         })
    return JsonResponse({"status":200,"message":"fail"}, safe=False)
