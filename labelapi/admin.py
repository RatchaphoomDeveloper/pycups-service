from django.contrib import admin
from .models import FileLogging
from .models import PrintLogging

admin.site.register(FileLogging)
admin.site.register(PrintLogging)
# Register your models here.
