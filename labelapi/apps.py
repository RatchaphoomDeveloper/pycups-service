from django.apps import AppConfig


class LabelapiConfig(AppConfig):
    name = 'labelapi'
