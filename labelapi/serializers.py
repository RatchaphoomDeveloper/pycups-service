from rest_framework import serializers
from .models import FileLogging
from .models import PrintLogging

class FileLoggingSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FileLogging
        fields = ('id','url','filename', 'dateGroup','message')


class PrintLoggingSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PrintLogging
        fields = ('id','url','filename', 'create_date','message')