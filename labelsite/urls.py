"""labelsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from labelapi import views
from rest_framework import routers

router = routers.DefaultRouter();
router.register('filelogging',views.FileLoggingView)
router.register('printlogging',views.PrintLoggingView)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/BarcodeGenerate',views.barcodeGenerate),
    path('printapi/PrintBarcode',views.barcodePrint),
    path('api/filelogging/',include(router.urls)),
    path('api/printlogging/',include(router.urls))
]
